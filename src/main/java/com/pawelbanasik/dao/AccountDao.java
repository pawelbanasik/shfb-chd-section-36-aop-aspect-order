package com.pawelbanasik.dao;

import org.springframework.stereotype.Component;

import com.pawelbanasik.domain.Account;

@Component
public class AccountDao {

	private String name;
	private String serviceCode;

	public void addAccount(Account account, boolean vipFlag) {
		System.out.println(getClass().getSimpleName() + ": Doing my db work: Adding and account.");
	}

	public boolean doWork() {
		System.out.println(getClass().getSimpleName() + ": in doWork()");
		return false;
	}

	public String getName() {
		System.out.println(getClass().getSimpleName() + ": in getName()");
		return name;
	}

	public void setName(String name) {
		System.out.println(getClass().getSimpleName() + ": in setName()");
		this.name = name;
	}

	public String getServiceCode() {
		System.out.println(getClass().getSimpleName() + ": in getServiceCode()");
		return serviceCode;
	}

	public void setServiceCode(String serviceCode) {
		System.out.println(getClass().getSimpleName() + ": in setServiceCode()");
		this.serviceCode = serviceCode;
	}

}
